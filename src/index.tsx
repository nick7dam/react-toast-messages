import * as React from 'react';
import './assets/style.scss'
import {Simulate} from "react-dom/test-utils";
import timeUpdate = Simulate.timeUpdate;
export interface IProps {
  status:string
  message: string,
  timeout: number
}
export interface IState {
    status:string
    message: string
    timeout: number
    show: boolean
}
export default class AwesomeToggleSwitch extends React.Component<IProps, IState> {
  constructor(props:any){
    super(props);
    this.state = {
        show: false,
        status: props.status ? props.status : '',
        message: props.message ? props.message : '',
        timeout: props.timeout ? props.timeout : ''
    };
  }
  componentDidUpdate(prevProps){
      if (this.props.message !== prevProps.message && this.props.message !== '') {
          this.start();
      }
  }
  start(){
      let self = this;
      this.setState({
          show: true
      });
     let int = setTimeout(function () {
         clearTimeout();
         console.info('this is the problem')
         self.closeToaster({})
        window.clearTimeout(int);
      }, self.state.timeout)
  }
  closeToaster(e){
    clearTimeout();
    this.setState({
        show: false,
        message: ''
    })
  }
  render(){
    let toRender = this.state.message !== '' || this.state.show ? <div className="toastMessage_wrapper">
        <div className="toastMessage toastMessage-show">
            <span className={"toastMessage_status toastMessage-"+ this.state.status}></span>
            <span className="toastMessage_message">{this.props.message}</span>
            <span className="toastMessage_close" onClick={(e)=>this.closeToaster(e)}>×</span></div>
    </div> : '';
    return(
      <div className='toastMessageContainer'>
          {toRender}
      </div>
    )
  }
}
