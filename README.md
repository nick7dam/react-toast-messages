# react-toast-messages

> ReactJS simple toast messages 

[![NPM](https://img.shields.io/npm/v/react-toast-messages.svg)](https://www.npmjs.com/package/react-toast-messages) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm i react-awesome-toggle-switch --save
```
## GitLab
https://gitlab.com/damjan89/react-toast-messages
## Usage
React >= 16.9.0
```tsx
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ReactToastMessages from 'react-toast-messages';
export interface IState {
    status:string,
    message: string,
    timeout: number,
}
export default class ReactToastMessagesExample extends React.Component<{}, IState> {
  constructor(props:any) {
      super(props);
      this.state = {
          status: 'danger', // success|danger|info|warning
          message: '',
          timeout: 2000
      }
  }
  componentDidMount(){

  }
  show(){
      let self = this;
          self.setState({
              message:'This is test toast message'
          });
          setTimeout(function () {
              self.setState({
                  message:''
              });
          }, this.state.timeout);
  }
  render() {
    return (
    <div style={{width: '100%'}}>
        <button onClick={()=>this.show()}>Show Toggle</button>
        <ReactToastMessages status={this.state.status} message={this.state.message} timeout={this.state.timeout}></ReactToastMessages>
    </div>
    );
  }
}
ReactDOM.render(<ReactToastMessagesExample/>, document.getElementById('root'));
```

## Preview
![preview](https://gitlab.com/damjan89/react-toast-messages/raw/master/success.png)
![preview](https://gitlab.com/damjan89/react-toast-messages/raw/master/danger.png)
![preview](https://gitlab.com/damjan89/react-toast-messages/raw/master/info.png)
![preview](https://gitlab.com/damjan89/react-toast-messages/raw/master/warning.png)


## License
MIT © [Nick Dam](https://gitlab.com/damjan89)
